﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mammoth.Models
{
	public class ItemsModel
	{
		public string ImageSource { get; set; }
		public string ItemName { get; set; }
		public int Price { get; set; }
	}
}
