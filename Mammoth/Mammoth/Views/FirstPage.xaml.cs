﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mammoth.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FirstPage : ContentPage
	{
		public FirstPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
		}
		protected override void OnAppearing()
		{
			base.OnAppearing();
			UserDialogs.Instance.Toast("Thanks for Choose our Application");
		}
		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			UserDialogs.Instance.Toast("You Are Leaving The MainPage");
		}

		private void ImageButton_Clicked(object sender, EventArgs e)
		{
			Navigation.PushAsync(new BreakFastPage());
		}

		private void ImageButton_Clicked_1(object sender, EventArgs e)
		{
			Navigation.PushAsync(new LunchPage());
		}

		private void ImageButton_Clicked_2(object sender, EventArgs e)
		{
			Navigation.PushAsync(new DinnerPage());
		}

		private void ImageButton_Clicked_3(object sender, EventArgs e)
		{
			Navigation.PushAsync(new DessertPage());
		}
	}
}