﻿using Mammoth.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mammoth.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DinnerPage : ContentPage
	{
		public DinnerPage()
		{
			InitializeComponent();
		}

		private void ItemList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var x = e.SelectedItem as ItemsModel;
			if (x == null)
			{ return; }
			else
			{
				Navigation.PushAsync(new DinnerOrderPage());
				ItemList.SelectedItem = null;
			}
		}
	}
}