﻿using Mammoth.Models;
using Mammoth.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Mammoth.ViewModels
{
	public class BreakFastViewModel:INotifyPropertyChanged
	{
		

		public BreakFastViewModel()
		{
			Test test = new Test();
			BreakFastItem = test.BreakFastItems;
		}


		private ObservableCollection<ItemsModel> _break;
		public ObservableCollection<ItemsModel> BreakFastItem { get { return _break; } set { _break = value; OnPropertyChanged(); } }
		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged(
	[CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this,
			new PropertyChangedEventArgs(propertyName));
		}
	}
}
